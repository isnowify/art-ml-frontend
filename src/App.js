import React, { useState } from 'react';
import axios from 'axios';
import { BrowserRouter as Router, Routes, Route, useNavigate } from 'react-router-dom';
import HandbookDisplay from './HandbookDisplay';
import { Analytics } from "@vercel/analytics/react"
import './App.css';

function Upload() {
  const [file, setFile] = useState(null);
  const [fileName, setFileName] = useState('');
  const [results, setResults] = useState([]);
  const [selectedItems, setSelectedItems] = useState([]);
  const [handbookData, setHandbookData] = useState(null);
  const [isLoading, setIsLoading] = useState(false); // Loading state
  const navigate = useNavigate();
  const endpointURL = 'http://127.0.0.1:5000/';

  const handleFileChange = (event) => {
    const file = event.target.files[0];
    if (file) {
      setFile(file);
      setFileName(file.name);
    }
  };

  const handleResultSelection = (item) => {
    const updatedSelection = selectedItems.includes(item)
        ? selectedItems.filter(selected => selected !== item)
        : selectedItems.length < 5 ? [...selectedItems, item] : selectedItems;
    setSelectedItems(updatedSelection);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    setIsLoading(true); // Set loading to true
    const formData = new FormData();
    formData.append('photo', file);

    try {
      const response = await axios.post(endpointURL + 'upload', formData);
      setResults(response.data.res); // Assuming this is the structure
      setIsLoading(false); // Reset loading after the request
    } catch (error) {
      console.error('Error uploading image:', error);
      setIsLoading(false); // Reset loading on error
    }
  };

  const handleFinalSubmit = async () => {
    setIsLoading(true); // Set loading to true
    try {
      const response = await axios.post(endpointURL + 'request', { selectedItems });
      setHandbookData(response.data); // Store data for navigation
      navigate('/handbook', { state: { handbookData: response.data } }); // Pass state via navigate
      setIsLoading(false); // Reset loading after navigation
    } catch (error) {
      console.error('Error submitting selected items:', error);
      setIsLoading(false); // Reset loading on error
    }
  };

  return (
      <div className="container">
        <Analytics />
        <h1>Upload Menu and Get Your Story!</h1>
        <form onSubmit={handleSubmit} className="upload-form">
          <label htmlFor="file-upload" className="file-upload-label">
            Choose a file
            <input id="file-upload" type="file" onChange={handleFileChange} className="file-input"/>
          </label>
          {fileName && (
              <div className="file-name-label">
                Selected file: {fileName}
              </div>
          )}
          <button type="submit" className="submit-btn" disabled={isLoading}>
            {isLoading ? 'Uploading...' : 'Upload and OCR'}
          </button>
        </form>

        {results.length > 0 && (
            <div>
              <h2>Select up to 5 items:</h2>
              <ul className="results-list">
                {results.map((result, index) => (
                    <li key={index} className={`result-item ${selectedItems.includes(result) ? 'selected' : ''}`}
                        onClick={() => handleResultSelection(result)}>
                      {result}
                    </li>
                ))}
              </ul>
              <button onClick={handleFinalSubmit} className="submit-btn">
                {isLoading ? 'Submitting...' : 'Submit Selections'}
              </button>
            </div>
        )}
      </div>
  );
}


function App() {
  return (
      <Router>
        <Routes>
          <Route path="/" element={<Upload />} />
          <Route path="/handbook" element={<HandbookDisplay />} />
        </Routes>
      </Router>

  );
}

export default App;
