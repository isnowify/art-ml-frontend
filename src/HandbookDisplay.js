import React, { useEffect, useRef } from 'react';
import { useLocation } from 'react-router-dom';
import Typist from 'react-typist-component';

function HandbookDisplay() {
    const location = useLocation();
    const handbookData = location.state?.handbookData;
    const audioRef = useRef(null);

    useEffect(() => {
        const audio = audioRef.current;
        if (audio) {
            audio.addEventListener('ended', () => {
                audio.play(); // Play the audio again once it ends
            });
            return () => {
                audio.removeEventListener('ended', () => {
                    audio.play();
                });
            };
        }
    }, []);

    if (!handbookData) {
        return (
            <div className="loading-container">
                <div className="loading-message loading-animation">Loading...</div>
            </div>
        );
    }


    return (
        <div className="loading-container">
            <audio ref={audioRef} src={handbookData.audio} autoPlay loop>
                Your browser does not support the audio element.
            </audio>
            <div className="handbook-card">
                <div className="handbook-image-container" style={{ flexDirection: 'column', overflowY: 'auto' }}>
                    {handbookData.img.map((imageSrc, index) => (
                        <img key={index} src={imageSrc} alt={`Dish Image ${index + 1}`} className="dish-image" />
                    ))}
                </div>
                <div className="handbook-text-container">
                    <Typist typingDelay={5} cursor={<span className='cursor'>|</span>}>
                        <br />
                        <Typist.Backspace count={5} />
                        {handbookData.conversation.map((entry, index) => (
                            <div key={index} className="conversation-entry">
                                <strong>{entry.role}:</strong> {entry.message}
                            </div>
                        ))}
                    </Typist>
                </div>
            </div>
        </div>
    );
}

export default HandbookDisplay;
